.data	
	laberinto: .dword 0x2b2d2d2d2d2d2d2b, 0x2b2d2d2d2d2d2d2d ,  0x20202020207c207c ,  0x7c2d20202020207c ,  0x202b2d2d207c207c ,  0x7c20202d2d2b207c ,  0x207c2020207c207c ,  0x7c202020207c207c ,  0x207c202d2d2b207c ,  0x2b2d2d20207c207c ,  0x207c20202058207c ,  0x7c232020207c2020 ,  0x2d2b2d2d2d2d2d2b ,  0x2b2d2d2d2d2b2d2d 
	//laberinto: .dword 0x2b2d2d2d2d2d2d2b, 0x2b2d2d2d2d2d2d2d ,  0x58202020207c207c ,  0x7c2d20202020207c ,  0x202b2d2d207c207c ,  0x7c20202d2d2b207c ,  0x207c2020207c207c ,  0x7c202020207c207c ,  0x207c202d2d2b207c ,  0x2b2d2d20207c207c ,  0x207c20202020207c ,  0x7c232020207c2020 ,  0x2d2b2d2d2d2d2d2b ,  0x2b2d2d2d2d2b2d2d
	//laberinto: .dword 0x2b2d2d2d2d2d2d2b, 0x2b2d2d2d2d2d2d2d ,  0x20202020207c207c ,  0x7c2d20202020207c ,  0x202b2d2d207c207c ,  0x7c20202d2d2b207c ,  0x207c2020207c207c ,  0x7c582020207c207c ,  0x207c202d2d2b207c ,  0x2b2d2d20207c207c ,  0x207c20202020207c ,  0x7c232020207c2020 ,  0x2d2b2d2d2d2d2d2b ,  0x2b2d2d2d2d2b2d2d
	estado: .dword 0x4e45204f4745554a, 0x21214f5352554320
	aux: .dword
	aux2: .dword
	_stack_ptr: .dword _stack_end   // Get the stack pointer value from memmap definition

.text	// Configuracion del Stack Pointer
	ldr     X1, _stack_ptr  
        mov     sp, X1          

	// Limpiar X0 y X4 siempre de comenzar el programa
    MOV X0, XZR
	MOV X4, XZR

	LDR X0, =laberinto //X0 = Dirección base del arreglo "laberinto"
	LDR X11,=aux	 // X11 = Dirección base del arreglo que copia y tambien sirve de arreglo auxiliar
	LDR X14, =estado // x14 = Dirección base del estado del juego
	LDR X26, =aux2   // x26 = Dirección base del segundo arreglo auxiliar para el conteo de pasos
	MOV W1, #0x58
	MOV W2, #0x20
	MOV W15, #0x23
	MOV W16, #0x59
	ADD X26, X26,#112    // Arreglo auxiliar 2  sumado 112  bits para que no interfiera en la copia
	MOV X19, #1          // contador de pasos 

main :	
	// Tienen un archivo comandosBreak para copiar todos y pegarlos en el dashboard
 	//para ver el laberinto dashboard memory watch 0x0000000040080488 128. 
	 //para ver la copia dashboard memory watch 0x0000000040080488 256
	 bl copy  // b *0x40080038
	 bl abajo  // b *0x4008003c
	 bl abajo  // b *0x40080040
	 bl abajo // b *0x40080044
	 bl abajo // b *0x40080048
	 bl derecha // b *0x4008004c
	 bl derecha  // b *0x40080050
	 bl derecha // b *0x40080054
	 bl derecha // b *0x40080058
	 bl arriba  // b *0x4008005c
	 bl arriba // b *0x40080060
	 bl izquierda // b *0x40080064
	 bl izquierda // b *0x40080068
	 bl arriba  // b *0x4008006c
	 bl arriba  // b *0x40080070
	 bl derecha // b *0x40080074
	 bl derecha // b *0x40080078
	 bl derecha // b *0x4008007c
	 bl derecha // b *0x40080080
	 bl abajo   // b *0x40080084
	 bl abajo   // b *0x40080088
	 bl abajo   // b *0x4008008c
	 bl abajo   // b *0x40080090
	 bl derecha //  b *0x40080094
	 bl derecha // b *0x40080098
	 bl arriba  // b *0x4008009c
	 bl arriba  // b *0x400800a0
	 bl arriba  // b *0x400800a4
	 bl arriba  // b *0x400800a8
	 bl derecha // b *0x400800ac
	 bl derecha //  b *0x400800b0
	 bl derecha // b *0x400800b4
	 bl derecha // b *0x400800b8
	 bl abajo   // b *0x400800bc
	 bl derecha // b *0x400800c0
	 bl abajo   // b *0x400800c4
	 bl izquierda // b *0x400800c8
	 bl izquierda // b *0x400800cc
	 bl abajo   // b *0x400800d0
	 bl abajo  // b *0x400800d4
	 bl derecha  // b *0x400800d8
	 bl derecha // b *0x400800dc
	 bl derecha //  b *0x400800e0
	

	abajo :
		ADD X20 , X30, XZR // X20 = Direccion de memoria que viene de X30 de bl abajo
		MOV X4, #0         // X4 = inicializacion del iterador i
		bl buscarpos       // buscarpos = funcion que busca la posicion de 58 
			
			bl replace     // replace = Remplaza el 58 por 20 
			
			ADD  X4, X4, #2  // Suma dos posiciones del i
			LSL X5, X4, #3
			ADD X5, X5,X0
			
			bl change       // change = funcion que guarda en memoria el 58 en la proxima posicion
		
	    br X20             // Salta a la siguien intruccion de movimiento

	arriba :
	    ADD X20 , X30, XZR	
		MOV X4, #0
		bl buscarpos
		
			bl replace

			SUB  X4, X4, #2  // resta dos posicion del i
			LSL X5, X4, #3
			ADD X5, X5,X0

			bl change

	br X20

	derecha :
		ADD X20 , X30, XZR
		MOV X4, #0
		bl buscarpos

		bl replace

		CMP X7, 0x00000000000000007   // si el J == 7 entonces entra en la condicion

		B.EQ if_right

		LSL X5, X4, #3  // si no se cumple la condicion anterior al i se lo deja fijo y al j se le suma 1 poscion
		ADD X5, X5,X0
		ADD X7, X7, #1

		next :          // next funcion donde siguie desp del salto de la condicion
		
			bl change

	br X20

	izquierda : 
		ADD X20 , X30, XZR
		MOV X4, #0
		bl buscarpos

		bl replace

		CBZ	 X7, if_left // si J == 0 entra en la condicion

		LSL X5, X4, #3
		ADD X5, X5,X0
		SUB X7, X7, #1  // i se lo deja fijo y j se le resta una posicion

	    next_2 :
			
			bl change

	br X20

//----------EJERCICIO 2----------------------------------------------//	

	copy :                 // copia el laberinto y los guarda en otro arreglo 
			LSL X5, X4, #3 
			ADD X5, X5,X0
			LDUR X10,[X5,#0]
			LSL X5, X4, #3 
			ADD X5, X5,X11
			STUR X10,[X5,#0]
			ADD X4, X4, #1
			CMP X4, 0x000000000000000e
			B.EQ steps     // terminado de copiar el laberinto salta a steps
	b copy

	steps :
			ADD X20 , X30, XZR
			MOV X4, #0
			bl buscarpos_2    // busca el primer 20
		search : 
			bl replace_2      // se reempalza el 20 por 59 (solo estetico)

			ADD  X5, X4, #2  // usamos x5 y x9 para guardar los valores de i y j y no cambiar su valor
			LSL X5, X5, #3
			ADD X5, X5,X11
			ADD X9, X7, XZR
			
			bl change_2		// saca los valores para la comparacion

			CMP W3, W1 
			B.EQ found_2   // si encuentra 58 salta found_2
			CMP W3, W2
			B.EQ down     // si encuentra el siguiente paso 20 salta si no se cumple sigue
			//-----
			SUB  X5, X4, #2   // siempre pisamos el registro x5 y x9 sin interferir con i(x4) y j(x9)
			LSL X5, X5, #3
			ADD X5, X5,X11
			ADD X9, X7, XZR

			bl change_2

			CMP W3, W1
			B.EQ found_2
			CMP W3, W2
			B.EQ up
			//-----
			CMP X7, 0x00000000000000007

			B.EQ if_1

			LSL X5, X4, #3
			ADD X5, X5,X11
			ADD X9, X7, #1

			bl change_2
			
			CMP W3, W1
			B.EQ found_2
			CMP W3, W2
			B.EQ right
		go_1 :
			//-----
			LSL X5, X4, #3
			ADD X5, X5,X11
			SUB X9, X7, #1
			
			bl change_2

			CMP W3, W1
			B.EQ found_2
			CMP W3, W2
			B.EQ left

		go_2 :

			down : 
				ADD X19, X19, #1 // x19 = contador de pasos anteriores hasta encontrar el 58
				ADD X4, X4, #2   // avanza  i+2 al siguiente paso
			b search             // salta search para encontrar 20 o 58
			up : 
				ADD X19, X19, #1
				SUB X4, X4, #2
			b search

			right : 
				ADD X19, X19, #1
				ADD X7, X7, #1
			b search

			right_1 : 
				ADD X19, X19, #1
				ADD X7, XZR, XZR
				ADD X4, X4, #1
			b search

			left :
				ADD X19, X19, #1
				SUB X7, X7, #1
			b search
			
			left_1 : 
				ADD X19, X19, #1
				MOV X7, #0x7
				SUB X4, X4, #1
			b search
b main

		
found_2 :
		LSL X19, X19, #2  // contador de paso lo multiplicamos por 4 para el salto a la siguiente instruccion
		ADD X20, X20, X19   // lo sumamos con X20 la direccion de memoria guardada para dar a la sigueinte instruccion
    br X20

if_1 :  
		ADD X5, X4, #1
		LSL X5, X5, #3
		ADD X5, X5, X11
		ADD X9,XZR,XZR

		bl change_2

		CMP W3, W1
		B.EQ found_2
		CMP W3, W2
		B.EQ right_1

b go_1

if_2 :  
		SUB X5, X4, #1
		LSL X5, X5, #3
		ADD X5, X5, X11
		MOV X9, #7 

		bl change_2

		CMP W3, W1
		B.EQ found_2
		CMP W3, W2
		B.EQ left_1

b go_2


		
buscarpos_2 : //Busca la posicion i y j
			MOV X7, #0
			LSL X5, X4, #3 // Iterador i
			ADD X5, X5,X11

			LDUR X10,[X5,#0]
			STUR X10, [X26,#0]
		while_2 :
			ADD X8, X7,X26 
			LDURB W6,[X8,#0] //Busca la posicion de 20
			CMP W6, 0x0000000000000058 // si encuentra a 58 primero que a 20 salta a return
			B.EQ return
			CMP W6, 0x0000000000000020
			B.EQ found
			ADD X7,X7,#1 //j
			CMP X7,0x0000000000000008
			B.EQ sum_2
		
		b while_2
		sum_2 : 
			ADD X4,X4,#1
		b buscarpos_2 

return : br X20 // va a la siguiente direccion de memoria


replace_2 : // reemplaza el 20 por un 59
		    STURB W16,[X8,#0]// La posicion donde estaba 20 la cambia por Y
			SUB X9, X7, X7
			ADD X8, X9,X26
			LDUR X10,[X8,#0]// guarda el valor cambiado en x10
			STUR X10,[X5,#0]// guarda el valor cambiado en la memoria

		br X30
change_2 :	

		LDUR X10,[X5,#0]
		STUR X10, [X26,#0] // SEGUNDO ARREGLO AUXILIAR
		ADD X8,X9,X26
		LDURB W3,[X8,#0]

	br X30
//----------EJERCICIO 1----------------------------------------------//	
	buscarpos : //Busca la posicion i y j
			MOV X7, #0
			LSL X5, X4, #3 // Iterador i
			ADD X5, X5,X0

			LDUR X10,[X5,#0]
			STUR X10, [X11,#0]
		while :
			ADD X8, X7,X11 
			LDURB W6,[X8,#0] //Busca la posicion de 58
			CMP W6, 0x0000000000000058 
			B.EQ found
			ADD X7,X7,#1 //j
			CMP X7,0x0000000000000008
			B.EQ sum
		
		b while
		sum : 
			ADD X4,X4,#1
		b buscarpos 

	found : br X30

	replace : // reemplaza el 58 por un 20
		    STURB W2,[X8,#0]// La posicion donde estaba 58 la cambia por 20
			SUB X9, X7, X7
			ADD X8, X9,X11
			LDUR X10,[X8,#0]// guarda el valor cambiado en x10
			STUR X10,[X5,#0]// guarda el valor cambiado en la memoria

		br X30
	
	change : //Cambia los valores del arreglo principal del laberinto moviendo una posicion a X
			LDUR X10,[X5,#0]  
			STUR X10, [X11,#0] //ARREGLO AUXILIAR
			ADD X8,X7,X11
			LDURB W3,[X8,#0]
			CMP W3, W15
			B.EQ ganaste
			CMP W3, W2
			B.NE perdiste
			STURB W1, [X8,#0]
			SUB X7,X7,X7
			ADD X8,X7,X11
			LDUR X10,[X8,#0]
			STUR X10,[X5,#0]

		br X30

	if_right : // si j = 7
		ADD X4, X4, #1
		LSL X5, X4, #3
		ADD X5, X5, X0
		ADD X7,XZR,XZR
	
	b next
	
	if_left :	//si j = 0
		SUB X4, X4, #1
		LSL X5, X4, #3
		ADD X5, X5, X0
		MOV X7, 0x7
	
	b next_2 



perdiste :     
		MOVZ X7, 0x4550, LSL 32 
		MOVK X7, 0x4452, LSL 48  
		STUR X7,[X14,#0]
		MOVZ X7, 0x5349, LSL 0
		MOVK X7, 0x4554, LSL 16 
		MOVK X7, 0x283A, LSL 48
		STUR X7,[X14,#8]
	b main
		
ganaste :	
	    MOVZ X7, 0x4147, LSL 16      
		MOVK X7, 0x414E, LSL 32      
		MOVK X7, 0x5453, LSL 48 
		STUR X7,[X14,#0]
		MOVZ X7, 0x2145, LSL 0 
		MOVK X7, 0x4220, LSL 16
		MOVK X7, 0x292D, LSL 32 
		STUR X7,[X14,#8]
	b main

/*
arriba:

abajo:

derecha:

izquierda:

ganaste:

perdiste:

*/
