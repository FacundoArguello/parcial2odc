.data	
	laberinto: .dword 0x2b2d2d2d2d2d2d2b, 0x2b2d2d2d2d2d2d2d ,  0x20202020207c587c ,  0x7c2d20202020207c ,  0x202b2d2d207c207c ,  0x7c20202d2d2b207c ,  0x207c2020207c207c ,  0x7c202020207c207c ,  0x207c202d2d2b207c ,  0x2b2d2d20207c207c ,  0x207c20202020207c ,  0x7c232020207c2020 ,  0x2d2b2d2d2d2d2d2b ,  0x2b2d2d2d2d2b2d2d 
	estado: .dword 0x4e45204f4745554a, 0x21214f5352554320
	aux: .dword 
	_stack_ptr: .dword _stack_end   // Get the stack pointer value from memmap definition

.text	// Configuracion del Stack Pointer
	ldr     X1, _stack_ptr  
        mov     sp, X1          
	// Limpiar X0 y X4 siempre de comenzar el programa
	MOV X0, XZR
	MOV X4, XZR

	LDR X0, =laberinto //X0 = Dirección base del arreglo "laberinto"
	LDR X11,=aux	 // X11 = Dirección base del arreglo "aux"
	LDR X14, =estado //x14= Dirección base del estado del juego
	MOV W1, #0x58 // X
	MOV W2, #0x20 //.
	MOV W15,#0x23 //#

main :
   //Para ver el laberinto  dashboard memory watch 0x0000000040080280 128 
    //tendran un archivo llamado ComandosBreakpoints con los breaks para copiar todos y pegarlos en el dashboard
	 bl abajo  //b *0x40080028	
	 bl abajo  //b *0x4008002c	
	 bl abajo  //b *0x40080030
	 bl abajo  //b *0x40080034	
	 bl derecha //b *0x40080038
	 bl derecha // b *0x4008003c	
	 bl derecha //b *0x40080040
	 bl derecha //b *0x40080044	
	 bl arriba  //b *0x40080048	
	 bl arriba   //b *0x4008004c	  
	 bl izquierda //b *0x40080050			
	 bl izquierda //b *0x40080054	
	 bl arriba  // b *0x40080058	
	 bl arriba   // b *0x4008005c
	 bl derecha   //  b *0x40080060
	 bl derecha    // b *0x40080064
	 bl derecha    // b *0x40080068
	 bl derecha   // b *0x4008006c
	 bl abajo   // b *0x40080070
	 bl abajo   // b *0x40080074
	 bl abajo   // b *0x40080078	
	 bl abajo   //b *0x4008007c	
	 bl derecha  //b *0x40080080	
	 bl derecha   // b *0x40080084	
	 bl arriba   //b *0x40080088
	 bl arriba	// b *0x4008008c
	 bl arriba	// b *0x40080090
	 bl arriba  // b *0x40080094	
	 bl derecha  //  b *0x40080098
	 bl derecha  // b *0x4008009c
	 bl derecha  // b *0x400800a0	
	 bl derecha  // b *0x400800a4
	 bl abajo   // b *0x400800a8	
	 bl abajo   //  b *0x400800ac
	 bl izquierda // b *0x400800b0
	 bl izquierda // b *0x400800b4
	 bl abajo  //  b *0x400800b8
	 bl abajo //  b *0x400800bc
	 bl derecha // b *0x400800c0	
	 bl derecha // b *0x400800c4	
	 bl derecha // b *0x400800c8	

	abajo :    //cuando movemos el "X" a una posicion, el arreglo se le suma 2 posiciones
		ADD X20 , X30, XZR  // x20 = guarda el salto de la direccion de x30
		MOV X4, #0  // x4 = inicializamo nuestro iterador i = 0
		bl buscarpos   // busca la poscion de 58 . devuelve x4 y x7 que hacen de i y j
			
			bl replace   // remplaza el 58 por 20 
			
			ADD  X4, X4, #2  // aumentamos nuestro  x4 dos posiciones  i+2
			LSL X5, X4, #3   // x5 = x4 *8
			ADD X5, X5,X0   // x5 = x4 + la dirección base del laberinto
			
			bl change     // Pone el 58 en la siguiente posición
		
	    br X20  // salto a la direccion a x20

	arriba :   // cuando movemos el "X" a una posicion, el arreglo se le resta 2 posiciones
	    ADD X20 , X30, XZR	
		MOV X4, #0
		bl buscarpos
		
			bl replace

			SUB  X4, X4, #2  // x4 = le restamos 2 posiciones
			LSL X5, X4, #3  
			ADD X5, X5,X0   

			bl change

	br X20

	derecha : 
		ADD X20 , X30, XZR
		MOV X4, #0
		bl buscarpos

		bl replace

		CMP X7, 0x00000000000000007 // En el caso si el iterador j == 7 saltamos a if right

		B.EQ if_right

		LSL X5, X4, #3  // dejamos el i fijo. x5 = x4 *8
		ADD X5, X5,X0
		ADD X7, X7, #1  // aumentamos una posicion hacia a la derecha 

		next :   // sigue la funcion despues del salto if_right
		
			bl change

	br X20

	izquierda : 
		ADD X20 , X30, XZR
		MOV X4, #0
		bl buscarpos

		bl replace

		CBZ	 X7, if_left //En el caso si el iterador j == 0 saltamos a if left

		LSL X5, X4, #3
		ADD X5, X5,X0
		SUB X7, X7, #1  // movemos una posicion a la izquierda

	    next_2 :
			
			bl change

	br X20

b main


//-----------------	
	buscarpos : //Busca la posicion i y j
			MOV X7, #0   // j
			LSL X5, X4, #3 // x5 = i*8
			ADD X5, X5,X0  // x5 = x5 + direccion base del laberinto

			LDUR X10,[X5,#0]  // x10 = laberinto[i]
			STUR X10, [X11,#0] // x11= arreglo auxiliar . aux [0] = x10
		while :
			ADD X8, X7,X11  // x8 = j + &aux[]
			LDURB W6,[X8,#0] // w6 = aux[j]. le da un byte
			CMP W6, 0x0000000000000058 //Busca la posicion de 58
			B.EQ found
			ADD X7,X7,#1 //j+1
			CMP X7,0x0000000000000008 // llega al limite salta y le suma a i una posicion
			B.EQ sum
		
		b while
		sum : 
			ADD X4,X4,#1
		b buscarpos 

	found : br X30	
	
	replace : // reemplaza el 58 por un 20
		    STURB W2,[X8,#0]// La posicion donde estaba 58 la cambia por 20. aux[j] = w2
			SUB X9, X7, X7  // x9=0
			ADD X8, X9,X11 // x8 = x9 + &aux[]
			LDUR X10,[X8,#0]// guarda el valor cambiado en x10, 8 bytes. x10 = aux[x8]
			STUR X10,[X5,#0]// guarda el valor cambiado en la memoria. lab[i] = x10

		br X30
	
	change : //Cambia los valores del arreglo principal del laberinto moviendo una posicion a X
			LDUR X10,[X5,#0] // x10 = lab[i]
			STUR X10, [X11,#0] // aux[0] = x10
			ADD X8,X7,X11 // x8 = j + &aux
			LDURB W3,[X8,#0] // w3 = aux[j]
			CMP W3, W15 // si el siguiente paso 23 ganaste
			B.EQ ganaste
			CMP W3, W2 //si el siguiente paso no es 20 perdiste
			B.NE perdiste
			STURB W1, [X8,#0] // cambia 20 por 58
			SUB X7,X7,X7
			ADD X8,X7,X11
			LDUR X10,[X8,#0]
			STUR X10,[X5,#0] // cambio total de los 8 bytes del laberinto

		br X30

	if_right : // si j = 7
		ADD X4, X4, #1 // agregamos una posicion del arreglo i+1
		LSL X5, X4, #3
		ADD X5, X5, X0
		ADD X7,XZR,XZR // j=0 
	
	b next
	
	if_left :	//si j = 0
		SUB X4, X4, #1 // se resta una posicion del arreglo i-1
		LSL X5, X4, #3
		ADD X5, X5, X0
		MOV X7, 0x7 // j=7
	
	b next_2 


perdiste :     
		MOVZ X7, 0x4550, LSL 32 // EP -> PE x7 = 0000 4550 0000 0000
		MOVK X7, 0x4452, LSL 48  // DR -> RD x7 = 4452 4550 0000 0000
		STUR X7,[X14,#0]  // estado [0] = x7S
		MOVZ X7, 0x5349, LSL 0 // SI -> IS x7 = 0000 0000 0000 5349
		MOVK X7, 0x4554, LSL 16  // ET -> TE  x7 = 0000 0000 4554 5349
		MOVK X7, 0x283A, LSL 48  // (: -> :(  x7 = 283A 0000 4554 5349
		STUR X7,[X14,#8] // estado[1] = x7
	b main
		
ganaste :	
	    MOVZ X7, 0x4147, LSL 16  // AG -> GA x7 = 0000 0000 4147 0000    
		MOVK X7, 0x414E, LSL 32  // AN -> NA x7 = 0000 414E 4147 0000
		MOVK X7, 0x5453, LSL 48  // TS -> ST x7 = 5453 414E 4147 0000
		STUR X7,[X14,#0] // estado [0]= x7
		MOVZ X7, 0x2145, LSL 0  // !E -> E! x7 = 0000 0000 0000 2145
		MOVK X7, 0x4220, LSL 16 // B- x7 = 0000 0000 4220 2145
		MOVK X7, 0x292D, LSL 32 //  x7 = 0000 292D 4220 2145
		STUR X7,[X14,#8]  // estado[1] = x7
	b main


